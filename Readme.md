PIONEER EPICS to tcp bridge for PiE5 magnet control at 2022 beamtime
====================================================================



Usage:
------

- Start:

~~~
python3 pioneerepics.py
~~~

- Connect using telnet:

~~~
telnet 127.0.0.1 5025
~~~

- Commands:

~~~
stop                     # - stop the program
restart                  # - stop and restart
setEpics name value      # space separated variablename and value to set
getEpics name            # name of variable to query
~~~

Will return "0\n" on success, "1\n" on error for now.
