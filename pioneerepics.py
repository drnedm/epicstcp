#!/usr/bin/python3

import socket
import time
import datetime
import sys
import select
import os
import logging
import json

from tcp import *

from epics import caget,caget_many,caput

# define 2 logs
# adapted from https://stackoverflow.com/questions/11232230/logging-to-two-files-with-different-settings
formatter = logging.Formatter('%(asctime)s.%(msecs)03d %(levelname)s %(message)s', datefmt='%Y-%m-%d_%H:%M:%S')

def setup_logger(name, log_file, level=logging.INFO):
    """To setup as many loggers as you want"""

    handler = logging.FileHandler(log_file)        
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)

    return logger

# first file logger
eventlog = setup_logger('eventlog', 'logs/events.log')

# second file logger
magnetlog = setup_logger('magnetlog', 'logs/magnets.log')

# read json files with accessible epics variables

readList = []
writeList = []

readconfigfilename = "configs/magnets.json"
if readconfigfilename:
    with open(readconfigfilename, 'r') as f:
        epicsjson = json.load(f)


for d in epicsjson["magnets"]:
    readList.append(d["name"]+d["sol"])
    readList.append(d["name"]+d["ist"])

    if(d["writable"]):
        writeList.append(d["name"]+d["sol"])

print("Successfully read magnet config from json")

#readconfigfilename = "configs/readAccess.json"
#if readconfigfilename:
#    with open(readconfigfilename, 'r') as f:
#        readepicsjson = json.load(f)
#
#writeconfigfilename = "configs/writeAccess.json"
#if writeconfigfilename:
#    with open(writeconfigfilename, 'r') as f:
#        writeepicsjson = json.load(f)

#print(readepicsjson["varnames"])

### Initialise port settings ###

tcpIp = '129.129.143.13'
tcpPort = 5025
bufferSize = 1024



### initial log of magnet data

#magnetlog.info(' '.join(readepicsjson["varnames"]))
magnetlog.info(' '.join(readList))
#string_currents = [str(current) for current in caget_many(readepicsjson["varnames"])]
string_currents = [str(current) for current in caget_many(readList)]
magnetlog.info(' '.join(string_currents))


### Create socket and connect to client

server = createServer(tcpIp, tcpPort, bufferSize)

stopped = False
haveLogged = False
while not stopped:
    try:
        conn, addr = server.accept() # accept connection from client
        print("\nConnection from " + addr[0] + "\n", flush=True)
        sys.stdout.flush()
        init = True
    except socket.timeout:
        if((haveLogged==False) and (time.localtime(time.time())[5]%10==0)):
            string_currents = [str(current) for current in caget_many(readList)]
            magnetlog.info(' '.join(string_currents))
            haveLogged=True
        elif (time.localtime(time.time())[5]%10!=0):
            haveLogged=False
        continue
    except:
        raise

    
    haveLogged = False
    try:
        while True:
            currentTime = int(time.time() * 10 ** 9)
    
            i, o, e = select.select([conn], [], [], 0.5) # check if socket input available
            if i:
                binString = conn.recv(bufferSize) # read socket input
    
                if binString.startswith(b'restart'): # restart socket
                    eventlog.info('Was commanded to restart')
                    restartProgram(server)
                elif binString.startswith(b'stop'): # stop program
                    eventlog.info('Was commanded to stop')
                    stopped=True
                    break
                elif binString.startswith(b'setEpics'): # set an EPICS variable to a value
                    parts = binString.split(b' ')
                    if(len(parts) == 3):
                        thisEpicsVar = parts[1].decode('utf-8').rstrip()
                        thisEpicsValue = float(parts[2].rstrip())
                        eventlog.info("Should set variable " + thisEpicsVar + " to " + str(thisEpicsValue))
                        print("Setting EPICS variable '" + thisEpicsVar + "' to " + str(thisEpicsValue))
                        if(thisEpicsVar in writeList):
                            if(caput(thisEpicsVar, thisEpicsValue)==1):
                                eventlog.info("caput of " + thisEpicsVar + " to " + str(thisEpicsValue) + " successful")
                                message = "OK\n"
                            else:
                                eventlog.info("caput of " + thisEpicsVar + " to " + str(thisEpicsValue) + " FAILED!")
                                print("caput ERROR!")
                                message = "CAPUT ERROR\n"
                        else:
                            eventlog.info("No write access to variable" + thisEpicsVar + "!")
                            print("no write access to var " + thisEpicsVar)
                            message = "ERROR\n"
                    else:
                        print("wrong 'setEpics' format!")
                        message = "ERROR\n"
                    conn.send(bytes(message, 'utf-8'))
                elif binString.startswith(b'getEpics'): # get an EPICS variable
                    nowTime = time.time()
                    parts = binString.split(b' ')
                    if(len(parts) == 2):
                        thisEpicsVar = parts[1].decode('utf-8').rstrip()
                        eventlog.info("Query for " + thisEpicsVar)
                        print("Getting EPICS variable '" + thisEpicsVar + "'")
                        if(thisEpicsVar in readList):
                            message = str(caget(thisEpicsVar)) + "\n"
                        else:
                            print("Epics variable not found!")
                            message = "ERROR\n"
                    else:
                        print("wrong 'getEpics' format!") 
                        message = "ERROR\n"
                    eventlog.info("Return " + message.rstrip())
                    conn.send(bytes(message, 'utf-8'))
                else: # default, print error message
                    print("received unknown: ",binString)
                    try:
                        eventlog.info("Received " + binString.decode('utf-8').rstrip())
                    except UnicodeDecodeError:
                        continue
                    except:
                        print("uncaught exception in logging/utf8 decoding")
                        raise
                    message = "ERROR\n"
                    eventlog.info("Return " + message.rstrip())
                    conn.send(bytes(message, 'utf-8'))
    
            else:
                if((haveLogged==False) and (time.localtime(time.time())[5]%10==0)):
                    string_currents = [str(current) for current in caget_many(readList)]
                    magnetlog.info(' '.join(string_currents))
                    haveLogged=True
                elif (time.localtime(time.time())[5]%10!=0):
                    haveLogged=False
                
                continue


    except socket.error as msg: # closing socket on lost connection
        print("Connection from " + addr[0] + " lost.", flush=True)
        sys.stdout.flush()
        restartProgram(server)
    
    except KeyboardInterrupt: # close socket on keyboard interrupt
        print("Program terminated.", flush=True)
        sys.stdout.flush()
    
print("done", flush=True)
sys.stdout.flush()
