import socket
import time
import sys
import select
import os

def createServer(tcpip, tcpport, buffersize):
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # create socket
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server.settimeout(0.2)
    print("\nSocket created.")
    try:
        server.bind((tcpip, tcpport)) # bind socket to IP and port
        print("\nBinding complete.")

    except OSError:
        print("Address already in use.")
        sys.exit()
    except socket.error as msg:
        print("Binding failed", msg[0], msg[1])
        sys.exit()

    try:
        server.listen(1) # waiting for client to connect
        print("\nSocket is listening for client on port ", tcpport , "...")

    except KeyboardInterrupt:
        server.shutdown(socket.SHUT_RDWR)
        server.close()
        print("Program terminated.")

    return server

def restartProgram(server): # restart python server script
    server.shutdown(socket.SHUT_RDWR)
    server.close()
    args = sys.argv[:]
    args.insert(0, sys.executable)
    os.execv(sys.executable, args)

